This python script can be used to extract colors and coordinates from a .h5 brain file and 
export them to a .json file that can be used by the brainvisualizer to display neurons
at pre-defined positions and colors.

It is also possible to specify multiple .h5 files coverting the same brain but at different resolutions. 
In this case, all the levels are merged in one files. The brainvisualizer will automatically choose the
correct level depending of the size of the brain.

